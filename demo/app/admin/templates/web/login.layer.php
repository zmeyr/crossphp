<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html;charset=UTF-8" />
	<title>后台管理系统</title>
    <link rel="stylesheet" type="text/css" href="<?php echo $this->res("css/style.css") ?>" />   
    <script type="text/javascript" src="<?php echo $this->res("js/lib/jquery.js") ?>"></script>
    <script type="text/javascript" src="<?php echo $this->res("lib/artDialog/artDialog.js?skin=idialog") ?>"></script>
    <script type="text/javascript" src="<?php echo $this->res("js/cp.js?v1") ?>"></script>
    <!--
    <script type="text/javascript" src="https://getfirebug.com/firebug-lite.js"></script> 
    -->
</head>
<body style="margin: 0px" scroll="no">
<div class="head">
</div>
<?php echo $content ?>
</body>
</html>
